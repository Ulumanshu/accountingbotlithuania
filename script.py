import click
from uber_rides.client import UberRidesClient
from uber_rides.auth import AuthorizationCodeGrant
import json

@click.command()
@click.option("--password1", prompt="1st password", help="1st password")
@click.option("--password2", prompt="2nd password", help="2nd password")
def get_auth(password1, password2):
    """Uber tokenizor"""
    auth_flow = AuthorizationCodeGrant(
        password1,
        {"history", "profile", "request", "request_receipt"},
        password2,
        "http://localhost",
    )
    auth_url = auth_flow.get_authorization_url()
    click.echo('Visit %s!' % auth_url)
    web = click.prompt("Paste in the adress you got redicected to after auth")
    session = auth_flow.get_session(web)
    client = UberRidesClient(session)
    credentials = session.oauth2credential
    response = client.get_user_profile()
    profile = response.json
    click.echo(profile)


def get_profile(auth_flow, web):
    session = auth_flow.get_session(web)
    client = UberRidesClient(session)
    credentials = session.oauth2credential
    response = client.get_user_profile()
    profile = response.json
    return profile

if __name__ == '__main__':
    auth_flow = get_auth()
    #web = click.prompt("Paste in the adress you got redicected to after auth")
    #singleton = get_profile(auth_flow, web)
    #click.echo(singleton)