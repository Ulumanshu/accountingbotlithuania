from uber_rides.client import UberRidesClient
from uber_rides.auth import AuthorizationCodeGrant
import json

"""Initialize AuthorizationCodeGrant Class.

Parameters
    client_id (str)
        Your app's Client ID.
    scopes (set)
        Set of permission scopes to request.
        (e.g. {'profile', 'history'}) Keep this list minimal so
        users feel safe granting your app access to their information.
    client_secret (str)
        Your app's Client Secret.
    redirect_url (str)
        The URL that the Uber server will redirect the user to after
        finishing authorization. The redirect must be HTTPS-based and
        match the URL you registered your application with. Localhost
        URLs are permitted and can be either HTTP or HTTPS.
    state_token (str)
        The CSRF State Token used to create an authorization.
"""


auth_flow = AuthorizationCodeGrant(
    password1,
    {"history", "profile", "request", "request_receipt"},
    password2,
    "http://localhost",
)

""" get_authorization_url()

       Start the Authorization Code Grant process.

       This function starts the OAuth 2.0 authorization process and builds an
       authorization URL. You should redirect your user to this URL, where
       they can grant your application access to their Uber account.

       Returns
           (str)
               The fully constructed authorization request URL.
               Tell the user to visit this URL and approve your app.
       """
auth_url = auth_flow.get_authorization_url()
print(auth_url)

x = input("Type X to continue: ")
while x != "X":
    x = input("Type X to continue")

response = input("Follow web link, copy current\n"
                 " address and paste it here: ")

session = auth_flow.get_session(response)
client = UberRidesClient(session)
credentials = session.oauth2credential

response = client.get_user_profile()
profile = response.json
print(profile)